package com.example.CA5.classes;

import com.example.CA5.models.Datas;

import java.util.ArrayList;

public class Term {
    public ArrayList<Grade> grades = new ArrayList<>();
    public double avg;
    public int number;

    Term(int _number) {
        number = _number;
        avg = 0.0;
    }

    public ArrayList<Grade> getGrades() {
        return grades;
    }

    public void addGrade(Grade grade){
        grades.add(grade);
    }

    public double getAvg() {
        int units = 0;
        for(Grade grade: grades){
            avg += (grade.getGrade() * grade.getUnits());
            units += grade.getUnits();
        }
        avg /= units;
        return avg;
    }

    public int getNumber() {
        return number;
    }

}
