package com.example.CA5.classes;

import static com.example.CA5.classes.Tools.courses;

public class MinJob implements Runnable {
    @Override
    public void run(){
        System.out.println("Hi");
        for(Course course : courses) {
            while(course.getWaitingStudentsSize() > 0 && course.getSignedUp() < course.getCapacity()){
                course.popWaiting();
            }
        }
    }
}
