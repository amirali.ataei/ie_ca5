package com.example.CA5.controller;

import com.example.CA5.classes.Course;
import com.example.CA5.models.*;
import com.example.CA5.classes.Student;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

import static com.example.CA5.classes.Tools.*;

@RestController
public class Controller {
    private Student myStudent;
    @RequestMapping(value = "/getStudent/{studentId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Std getGetStudent(@PathVariable(value = "studentId") String id) {
        Student student = null;
        Std std = new Std();
        System.out.println(id);
        try {
            student = getStudent(id);
            myStudent = student;
            std.setId(id);
            std.setName(student.getName());
            std.setSecondName(student.getSecondName());
            std.setBirthDate(student.getSecondName());
            std.setField(student.getField());
            std.setFaculty(student.getFaculty());
            std.setStatus(student.getStatus());
            std.setLevel(student.getLevel());
            std.setImg(student.getImg());
            std.setTerms(student.getTerms());
            std.setGpa(student.getGPA());
            std.setPassedUnits(student.getNumOfPassedUnits());
            std.setStatusCode(202);
            studentId = id;

        } catch (Exception e) {
            std.setStatusCode(404);
        }
        assert student != null;
        System.out.println(student.getName());
        return std;
    }

    @RequestMapping(value = "/getSelectedCourses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Crs getGetSelectedCourses() {

        Crs crs = new Crs(selectedCourses);
        crs.setStatusCode(202);
        return crs;
    }

    @RequestMapping(value = "/getUnits", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Units getUnits() {
        int totalUnits = myStudent.getWeeklySchedule().getTotalUnits();
        Units units = new Units(totalUnits);
        units.setStatusCode(202);
        return units;
    }

    @RequestMapping(value = "/getCourses/{filter}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Crs getGetCourses(@PathVariable(value = "filter") String _filter) {

        ArrayList<Course> filterCourses = courses;
        searchFilter = _filter;
        filterCourses = filter();
        Crs crs = new Crs(filterCourses);
        crs.setStatusCode(202);
        return crs;
    }

    @RequestMapping(value = "/submitCourses", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Datas putSubmitCourses() {
        Datas datas = new Datas();
        try {
            myStudent.setWeeklySchedule(selectedCourses);
            myStudent.finalizeWeeklySchedule();
            lastSubmit = new ArrayList<>(selectedCourses);
            datas.setStatusCode(202);
        } catch (Exception e) {
            datas.setStatusCode(405);
        }
        return datas;
    }

    @RequestMapping(value = "/addCourse", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Datas postAddCourse(@RequestParam(value = "course_code") String code,
                               @RequestParam(value = "class_code") String classCode) {
        Datas datas = new Datas();
        Course course = null;
        try {
            course = getCourse(code, classCode);
        } catch (Exception e) {
            datas.setStatusCode(404);
        }
        try {
            myStudent.addToWeeklySchedule(course);
            selectedCourses.add(course);
            datas.setStatusCode(202);
        } catch (Exception e) {
            datas.setStatusCode(405);
        }
        return datas;
    }

    @RequestMapping(value = "/removeCourse", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Datas deleteRemoveCourse(@RequestParam(value = "course_code") String code,
                               @RequestParam(value = "class_code") String classCode) {

        Datas datas = new Datas();
        Course course = null;

        try {
            course = getCourse(code, classCode);
        } catch (Exception e) {
            datas.setStatusCode(404);
        }

        try {
            myStudent.removeFromWeeklySchedule(course);
            selectedCourses.remove(course);
            datas.setStatusCode(202);
        } catch (Exception e) {
            datas.setStatusCode(405);
        }
        return datas;
    }

    @RequestMapping(value = "/waitCourse", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Datas postWaitCourse(@RequestParam(value = "course_code") String code,
                                    @RequestParam(value = "class_code") String classCode) {

        Datas datas = new Datas();
        Course course = null;
        try {
            course = getCourse(code, classCode);
        } catch (Exception e) {
            datas.setStatusCode(404);
        }

        try {
            myStudent.addToWeeklySchedule(course);
            course.setWaiting(true);
            selectedCourses.add(course);
            datas.setStatusCode(202);
        } catch (Exception e) {
            datas.setStatusCode(405);
        }
        return datas;
    }

    @RequestMapping(value = "/resetCourses", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Datas putResetCourses() {

        Datas datas = new Datas();

        myStudent.setWeeklySchedule(lastSubmit);
        selectedCourses = new ArrayList<>(lastSubmit);

        datas.setStatusCode(202);
        return datas;
    }

    @RequestMapping(value = "/getSchedule", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Days getSchedule() {
        String[] times = new String[5];
        times[0] = "7:30-9:00";
        times[1] = "9:00-10:30";
        times[2] = "10:30-12:00";
        times[3] = "14:00-15:30";
        times[4] = "16:00-17:30";

        String[] sat = new String[5];
        String[] sun = new String[5];
        String[] mon = new String[5];
        String[] tue = new String[5];
        String[] wed = new String[5];
        for(int i = 0; i < 5; i++) {
            sat[i] = "";
            sun[i] = "";
            mon[i] = "";
            tue[i] = "";
            wed[i] = "";
        }

        for (Course course : lastSubmit){
            String name = course.getName();
            String time =  (String) course.getClassTime().get("time");
            ArrayList<String> days = (ArrayList<String>) course.getClassTime().get("days");
            for(int i = 0; i < 5; i++) {
                if (time.equals(times[i])) {
                    if (days.get(0).equals("Saturday") || days.get(1).equals("Saturday"))
                        sat[i] = name;
                    if (days.get(0).equals("Sunday") || days.get(1).equals("Sunday"))
                        sun[i] = name;
                    if (days.get(0).equals("Monday") || days.get(1).equals("Monday"))
                        mon[i] = name;
                    if (days.get(0).equals("Tuesday") || days.get(1).equals("Tuesday"))
                        tue[i] = name;
                    if (days.get(0).equals("Wednesday") || days.get(1).equals("Wednesday"))
                        wed[i] = name;
                }
            }
        }
        Days d = new Days(sat, sun, mon, tue, wed);
        d.setStatusCode(202);
        return d;
    }
}
