package com.example.CA5.models;

import com.example.CA5.classes.Term;

public class Std extends Datas{
    public String id;
    public String name;
    public String secondName;
    public String birthDate;
    public String field;
    public String faculty;
    public String level;
    public String status;
    public String img;
    public Term[] terms = new Term[12];
    public double gpa;
    public int passedUnits;

    public void setId(String _id) {
        id = _id;
    }

    public void setName(String _name) {
        name = _name;
    }

    public void setSecondName(String _secondName) {
        secondName = _secondName;
    }

    public void setBirthDate(String _birthDate) {
        birthDate = _birthDate;
    }

    public void setField(String _field) {
        field = _field;
    }

    public void setFaculty(String _faculty) {
        faculty = _faculty;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setTerms(Term[] terms) {
        this.terms = terms;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }

    public void setPassedUnits(int passedUnits) {
        this.passedUnits = passedUnits;
    }
}
