// class LoginForm extends React.Component{
//     constructor(props) {
//         super(props);
//         this.handleLogin = this.handleLogin.bind(this);
//         this.goToHome = this.goToHome.bind(this);
//         document.body.className = 'login_page';
//
//         this.state = {studentId: '', password: ''};
//         this.handleId = this.handleId.bind(this);
//         this.handlePass = this.handlePass.bind(this);
//     }
//
//     render() {
//         return(
//             <>
//                 <img src="/images/logo.png" />
//                 <form className="login_form" id="login-form" onSubmit={this.goToHome}>
//                     <label>ایمیل:</label>
//                     <input type="text" name="std_id" onChange={this.handleId}/><br/><br/>
//                     <label>رمز عبور:</label>
//                     <input type="password" name="pass" onChange={this.handlePass}/><br/><br/>
//                     <button type="submit">ورود</button>
//                 </form><br/><br/><br/><br/><br/>
//                 </>
//         );
//     }
//
//     handleId(event) {
//         this.setState({studentId: event.target.value});
//     }
//
//     handlePass(event) {
//         this.setState({password: event.target.value});
//     }
//
//     goToHome(event) {
//         event.preventDefault();
//         ReactDOM.render(<BodySection std_id={this.state.studentId}/>, document.getElementById('app'));
//     }
//
//     handleLogin() {
//         // event.preventDefault();
//         // var params = {
//         //     "std_id": this.props.std_id,
//         //     "password" : this.props.password,
//         // };
//         // var queryString = Object.keys(params).map(function(key) {
//         //     return key + '=' + params[key]
//         // }).join('&');
//         // const requestOptions = {
//         //     method: 'POST',
//         //     headers: {
//         //         'content-length' : queryString.length,
//         //         'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
//         //     },
//         //     body: queryString
//         // };
//         this.goToHome();
//         const form = new FormData(document.getElementById('login-form'));
//         fetch('/login', {
//             method: 'POST',
//             body: form
//         });
//
//         // fetch('/', requestOptions)
//         //     .then(response => response.json());
//         //     // .then(data => $("#infoModal").modal("hide"));
//     }
//
// }