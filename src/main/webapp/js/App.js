function Footer() {
    return(
        <footer>
            <i className="flaticon-copyright" />
            دانشگاه تهران - سامانه جامع بلبل‌ستان
            <i className="flaticon-facebook" />
            <i className="flaticon-linkedin-logo"/>
            <i className="flaticon-instagram"/>
            <i className="flaticon-twitter-logo-on-black-background"/>
        </footer>
    );
}

function Menu() {
    return(
        <BrowserRouter>
            <Switch>
                <Route path="/login" component={LoginForm}></Route>
                <Route path="/courses" component={Courses}></Route>
                <Route path="/plan" component={Plan}></Route>
                <Route path="/" exact component={BodySection}></Route>
            </Switch>
        </BrowserRouter>
    );
}

class LoginForm extends React.Component{
    constructor(props) {
        super(props);
        this.handleLogin = this.handleLogin.bind(this);
        this.goToHome = this.goToHome.bind(this);
        document.body.className = 'login_page';

        this.state = {studentId: '', password: ''};
        this.handleId = this.handleId.bind(this);
        this.handlePass = this.handlePass.bind(this);
    }

    render() {
        return(
            <>
                <img src="/images/logo.png" />
                <form className="login_form" id="login-form" onSubmit={this.goToHome}>
                    <label>ایمیل:</label>
                    <input type="text" name="std_id" onChange={this.handleId}/><br/><br/>
                    <label>رمز عبور:</label>
                    <input type="password" name="pass" onChange={this.handlePass}/><br/><br/>
                    <button type="submit">ورود</button>
                </form><br/><br/><br/><br/><br/>
            </>
        );
    }

    handleId(event) {
        this.setState({studentId: event.target.value});
    }

    handlePass(event) {
        this.setState({password: event.target.value});
    }

    goToHome(event) {
        event.preventDefault();
        ReactDOM.render(<BodySection std_id={this.state.studentId}/>, document.getElementById('app'));
    }

}

class BodySection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {student: {}};
    }

    render() {
        return (
            <>
            <div className="navbar">

                <img src="/images/logo.png" id="logo"/>
                    <Link to="/courses">انتخاب واحد</Link>
                    <Link to="/plan">برنامه هفتگی</Link>
                    <Link to="/logout">خروج<i className="flaticon-log-out"/></Link>
            </div>

        <img id="cover" src="images/cover%20photo.jpg"/>

                <section>
                    <nav>
                        <Info student={this.state.student} />
                    </nav>

                    <article>
                        <fieldset>
                            <Report terms={this.state.student.terms}/>
                        </fieldset>
                    </article>
                </section>
            </>

        );
    }

    async componentDidMount() {
        const data = await fetch('/getStudent/' + this.props.std_id).then(response => response.json());
        this.setState({student: data});
        console.log(this.state);
    }
}



class Info extends React.Component { //props have to be a Student.
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <>
                <img id="profile" src={this.props.student.img} />
                <table>
                    <tr>
                        <td> نام: </td>
                        <td> {this.props.student.name} </td>
                    </tr>
                    <tr>
                        <td> نام خانوادگی: </td>
                        <td> {this.props.student.secondName} </td>
                    </tr>
                    <tr>
                        <td> شماره دانشجویی: </td>
                        <td> {this.props.student.id} </td>
                    </tr>
                    <tr>
                        <td> تاریخ تولد: </td>
                        <td> {this.props.student.birthDate} </td>
                    </tr>
                    <tr>
                        <td> معدل کل: </td>
                        <td> {this.props.student.gpa} </td>
                    </tr>
                    <tr>
                        <td> واحد گذرانده: </td>
                        <td> {this.props.student.passedUnits} </td>
                    </tr>

                    <tr>
                        <td> دانکشده: </td>
                        <td> {this.props.student.faculty} </td>
                    </tr>
                    <tr>
                        <td> رشته: </td>
                        <td> {this.props.student.field} </td>
                    </tr>
                    <tr>
                        <td> مقطع: </td>
                        <td> {this.props.student.level} </td>
                    </tr>
                </table>
                <p id="status">{this.props.student.status}</p>
            </>
        );
    }
}

class CourseInReport extends React.Component { //props have to be a Grade.
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <table>
                <tr>
                    <td>
                        {this.props.index}
                    </td>
                    <td>
                        {this.props.grade.code}
                    </td>
                    <td>
                        {this.props.grade.name}
                    </td>
                    <td>
                        {this.props.grade.unit} واحد
                    </td>
                    <td>
                        <p className={this.props.grade.grade >= 10 ? "pass" : "fail"}>
                            {this.props.grade.grade >= 10 ? "قبول" : "مردود"}
                        </p>
                    </td>
                    <td className={this.props.grade.grade >= 10 ? "pass2" : "fail2"}>
                        {this.props.grade.grade}
                    </td>
                </tr>
            </table>
        );
    }
}

class TermInReport extends React.Component{ //props have to be a Term.
    constructor(props) {
        super(props);
    }

    render() {
        return(
            this.props.grades.map((grade, index) => (
                <CourseInReport grade={grade} index={index}/>
            ))
        );
    }

}

class Report extends React.Component{ //props have to be a Student.
    constructor(props) {
        super(props);
    }

    render() {
        return(
            this.props.terms.map(term => (
                <>
                    <legend> کارنامه-ترم {term.number} </legend>
                    <TermInReport grades={term.grades} />
                    <p className="avg">معدل: {term.avg}</p>
                </>
                ))
            );
    }


}

class Courses extends React.Component {
    constructor(props) {
        super(props);
        this.state = {courses: [], selectedCourses: []};
    }

    render() {
        return (
            <>
                <div>
                    <fieldset>
                    <legend>
                        دروس انتخاب شده
                    </legend>
                    <SelectedCourses courses={this.state.selectedCourses} />
                    <hr class="line"/>
                    <SubmitForm />
                    </fieldset>
                </div>

                <br/>
                <SearchForm />
                <br/>

                <fieldset>
                    <legend>دروس ارائه شده</legend>
                    <AllCourses courses={this.state.courses}/>
                </fieldset>
            </>
        );
    }

    async componentDidMount() {
        const data = await fetch('/getCourses/').then(response => response.json());
        this.setState({courses: data});
        console.log(this.state);

        const data2 = await fetch("/getSelectedCourses").then(response => response.json());
        this.setState({selectedCourses: data2});
        console.log(this.state);
    }

}


class CourseInSelected extends React.Component{
    constructor(props) {
        super(props);
        this.removeCourse = this.removeCourse.bind(this);
    }

    render() {
        return(<tr>
            <td>
                <form action="" method="DELETE" id="delete" onSubmit={removeCourse}>
                    <input id="form_action" type="hidden" name="action" value="remove"/>
                    <input id="form_course_code" type="hidden" name="course_code" value={this.props.course.code}/>
                    <input id="form_class_code" type="hidden" name="class_code" value={this.props.course.classCode}/>
                    <button type="submit"><i className="flaticon-trash-bin"/></button>
                </form>
            </td>
            <td><p className={this.props.course.waiting ? "waiting" : this.props.course.status ? "submitted" : "notSubmitted"}>
                {this.props.course.waiting ? "در انتظار" : this.props.course.status ? "ثبت  شده" : "ثبت نهایی نشده"}
            </p></td>
            <td>{this.props.course.code + '-' + this.props.course.classCode}</td>
            <td>{this.props.course.name}</td>
            <td>{this.props.course.instructor}</td>
            <td>{this.props.course.units}</td>
        </tr>);
    }

    removeCourse() {
        const form = new FormData(document.getElementById("delete"));
        fetch(
            '/removeCourse',
            {
                method: "DELETE",
                body: form
            }
        ).then(r => r.json());
    }
}

function SCS(){
    return (<tr>
        <th>
        </th>
        <th>
            وضعیت
        </th>
        <th>
            کد
        </th>
        <th>
            نام درس
        </th>
        <th>
            استاد
        </th>
        <th>
            واحد
        </th>
    </tr>);
}

class SelectedCourses extends React.Component{ //props have to be a Student.
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <table className = "selected-courses">
                <SCS />
                {this.props.courses.map(course => (
                    <CourseInSelected course={course}/>
                ))}
            </table>
        );
    }

}

class SubmitForm extends React.Component{ //props have to be a Student.
    constructor(props) {
        super(props);
        this.state = {units: 0};
        this.submitCourses = this.submitCourses.bind(this);
        this.resetCourses = this.resetCourses.bind(this);
    }

    render() {
        return (<form action="" method="PUT" className="submit_reset" id="submit_reset">
            {this.state.units}
            <button type="submit" name="action" value="submit" className="submit" onSubmit={submitCourses}>ثبت نهایی</button>
            <button type="submit" name="action" value="reset" className="reset" onSubmit={resetCourses}>
                <i className="flaticon-refresh-arrow"/>
            </button>
        </form>);
    }

    submitCourses() {
        const form = new FormData(document.getElementById("submit_reset"));
        fetch(
            '/submitCourses',
            {
                method: "PUT",
                body: form
            }
        ).then(r => r.json());
    }

    resetCourses() {
        const form = new FormData(document.getElementById("submit_reset"));
        fetch(
            '/resetCourses',
            {
                method: "PUT",
                body: form
            }
        ).then(r => r.json());
    }

    async componentDidMount() {
        const data = await fetch('/getUnits').then(response => response.json());
        this.setState({units: data});
        console.log(this.state);
    }
}

class SearchForm extends React.Component{
    constructor(props) {
        super(props);
        this.handleSearch = this.handleSearch.bind(this);
        this.getCourses = this.getCourses.bind(this);
        this.state = {search: ''};
    }

    render() {
        return (
            <form className="search_form" action="" method="GET" onSubmit={this.getCourses}>
            <input type="text" name="search" placeholder="نام درس" onChange={this.handleSearch}/>
            <button type="submit" name="action" value="search">
                جستجو<i className="flaticon-loupe"/>
            </button>
            </form>
        );
    }

    handleSearch(event) {
        this.setState({search: event.target.value});
    }

    getCourses(event) {
        const data = fetch(
            '/getCourses/' + this.state.search
        ).then(r => r.json());
    }

}

function ACS(){
    return (
        <tr>
        <th/>
        <th>کد</th>
        <th>ظرفیت</th>
        <th>نوع</th>
        <th>نام درس</th>
        <th>استاد</th>
        <th>واحد</th>
        <th>پیشنیازها</th>
        </tr>
    );
}

class AllCourses extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
                <table className="subject">
                    <tr>
                        <td><a href="#all">همه</a></td>
                        <td><a href="#special">اختصاصی</a></td>
                        <td><a href="#main">اصلی</a></td>
                        <td><a href="#basic">پایه</a></td>
                        <td><a href="#public">عمومی</a></td>
                    </tr>
                </table>
                <br/>
                <table className="course_table">
                    <ACS />
                    {this.props.courses.map(course => (
                        <CourseInAll course={course} />
                    ))}
                </table>
            </>
        );
    }
}

class CourseInAll extends React.Component{ //props have to be a Course
    constructor(props) {
        super(props);
        this.waitCourse = this.waitCourse.bind(this);
        this.addCourse = this.addCourse.bind(this);
    }

    render() {
        return (
            <tr>
                <td>
                    <form action="" method="POST" onSubmit={this.props.course.capacity >= this.props.course.signedUp ? this.waitCourse : this.addCourse} id="add_wait">
                        <input id="form_action1" type="hidden" name="action" value="add"/>
                        <input id="form_course_code1" type="hidden" name="course_code" value={this.props.course.code}/>
                        <input id="form_class_code1" type="hidden" name="class_code" value={this.props.course.classCode}/>
                        <button type="submit" className={this.props.course.capacity >= this.props.course.signedUp ? "wait" : "add"}>
                            <i className={this.props.course.capacity >= this.props.course.signedUp ? "flaticon-clock-circular-outline" : "flaticon-add"}/>
                        </button>
                    </form>
                </td>
                <td>{this.props.course.code}-{this.props.course.classCode}</td>
                <td className={this.props.course.capacity >= this.props.course.signedUp ? "full" : "notFull"}>{this.props.course.signedUp}/{this.props.course.capacity()}</td>
                <td>
                    <div className={"type " + this.props.course.type}>{this.props.course.type}</div>
                </td>
                <td>{this.props.course.name}</td>
                <td>{this.props.course.instructor}</td>
                <td className="notFull">{this.props.course.units}</td>
                <td/>
            </tr>
        );
    }

    addCourse() {
        const form = new FormData(document.getElementById("add_wait"));
        fetch(
            '/addCourse',
            {
                method: "POST",
                body: form
            }
        ).then(r => r.json());
    }

    waitCourse() {
        const form = new FormData(document.getElementById("add_wait"));
        fetch(
            '/waitCourse',
            {
                method: "POST",
                body: form
            }
        ).then(r => r.json());
    }
}