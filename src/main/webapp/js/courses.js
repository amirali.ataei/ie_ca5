function courseInSelected(props){ //props have to be a Course
    return(<tr>
            <td>
                <form action="" method="POST" >
                    <input id="form_action" type="hidden" name="action" value="remove"/>
                    <input id="form_course_code" type="hidden" name="course_code" value={props.getCode()}/>
                    <input id="form_class_code" type="hidden" name="class_code" value={props.getClassCode()}/>
                    <button type="submit"><i class="flaticon-trash-bin"></i></button>
                </form>
            </td>
            <td><p class="submitted">{props.getStatus()}</p></td>
            <td>{props.getCode()}</td>
            <td>{props.getName()}</td>
            <td>{props.getInstructor()}</td>
            <td>{props.getUnits()}</td>
        </tr>);
}

function courseInAll(props){ //props have to be a Course
    return(<tr>
                <td>
                    <form action="" method="POST">
                        <input id="form_action1" type="hidden" name="action" value="add" />
                        <input id="form_course_code1" type="hidden" name="course_code" value={props.getCode()} />
                        <input id="form_class_code1" type="hidden" name="class_code" value={props.getClassCode()} />
                        <button type="submit" class="add"><i class="flaticon-add"></i></button>
                    </form>
                </td>
                <td>{props.getCode()}-{props.getClassCode()}</td>
                <td class="notFull">{props.getSignedUp()}/{props.getCapacity()}</td>
                <td><div class="type public">{props.getType()}</div></td>
                <td>{props.getName()}</td>
                <td>{props.getInstructor()}</td>
                <td class="notFull">{props.getUnits()}</td>
                <td>{props.getPrerequisites()}</td>
           </tr>
    );
}

function sCS(props){
    var i;
    var res=(<tr>
                 <th>
                 </th>
                 <th>
                    وضعیت
                 </th>
                 <th>
                     کد
                 </th>
                 <th>
                     نام درس
                 </th>
                 <th>
                    استاد
                 </th>
                 <th>
                     واحد
                 </th>
             </tr>);
    for(i in props)
        res=res+courseInSelected(i);
    return res;
}

function selectedCourses(props){ //props have to be a Student.
    return(<table class="selected-courses">{sCS(props.getWeeklySchedule().gWS())}</table>); //this getWeeklySchedule is selected Courses(buffer plan)
}

function submitForm(props){ //props have to be a Student.
    return(<form action="" method="POST" class="submit_reset">
                {props.getWeeklySchedule().getTotalUnits()}
                <button type="submit" name="action" value="submit" class="submit">ثبت نهایی</button>
                <button type="submit" name="action" value="reset" class="reset"><i class="flaticon-refresh-arrow"></i></button>
           </form>);
}

function searchForm(){
    return(<form class="search_form" action="" method="POST">
                <input type="text" name="search" value="" placeholder="نام درس" />
                <button type="submit" name="action" value="search">جستجو<i class="flaticon-loupe"></i></button>
           </form>);
}

function aCS(props){
    var i;
    var res=(<tr>
                 <th></th>
                 <th>کد</th>
                 <th>ظرفیت</th>
                 <th>نوع</th>
                 <th>نام درس</th>
                 <th>استاد</th>
                 <th>واحد</th>
                 <th>پیشنیازها</th>
             </tr>);
    for(i in props)
        res=res+courseInSelected(i);
    return res;
}

function allCourses(props){
    return(
        <>
        <table class="subject">
                <tr>
                    <td><a href="#all">همه</a></td>
                    <td><a href="#special">اختصاصی</a></td>
                    <td><a href="#main">اصلی</a></td>
                    <td><a href="#basic">پایه</a></td>
                    <td><a href="#public">عمومی</a></td>
                </tr>
           </table>
           <br/>
           <table class="course_table">{sCS(props.getWeeklySchedule().gWS())}</table>
           </>
    );
}